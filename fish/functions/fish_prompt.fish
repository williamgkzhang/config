function git_symbols
  git diff --no-ext-diff --quiet || set gitState $gitState "*"
  git diff --no-ext-diff --cached --quiet || set gitState $gitState "+"
  if test (git_untracked_files) -gt 0 
    set gitState $gitState "?"
  end

  if test (count $gitState) -gt 0
    printf " %s" (string join "" $gitState)
  end
end

function git_prompt
  if git_is_repo
    set_color -b 005377
    set_color white
    printf ' '
    printf (string trim (git_branch_name))
    git_symbols
    printf ' '
    set_color -b normal
    set_color 005377
    printf ''
  end
end

function short_pwd
  string replace "$HOME" "~" (pwd)
end

function fish_prompt
  set gitPrompt (string trim (git_prompt))

  set_color -b 222
  #printf "%s" (whoami) ':'
  set_color green
  printf "%s" (short_pwd) ' '
  if test (count $gitPrompt) -gt 0
    set_color -b 005377
    set_color 222
    printf ''
    printf "%s" $gitPrompt
  else
    set_color -b normal
    set_color 222
    printf ''
  end
  set_color normal
  printf ' '
end
